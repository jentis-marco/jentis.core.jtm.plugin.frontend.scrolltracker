	function(jtsTracker,oGateConfig) {

	this.oTracker = jtsTracker;
	this.oGateConfig = oGateConfig;

	this.init = function(){
		this.oTracker.registerGateFunc("pageview",this.pageview,this);
	}
	
	this.pageview = function(){
		this.oConf = this.setupConfiguration(this.oGateConfig);

		this.handleScrollEvent = function(e) {
			window.removeEventListener("scroll", this.handleScrollEvent);
			window.setTimeout(function() {
				var currentScrollPosition = Math.ceil(window.scrollY + window.innerHeight); // We need the bottom edge of the window, almost the same as window.pageYOffset
				var docHeight = Math.floor(document.body.clientHeight);
				var scrollPosRelative = currentScrollPosition / docHeight;
	
				for (var i=0; i < this.oConf.values.length; i++) {
					if ((this.oConf.values[i]/100) > scrollPosRelative) {
						break;
					}
	
					if (this.oConf.sent[i] !== true) {
						var nonInteraction = true;

						// We check the configuration when should we consider the event interactive
						// if no value is set, then event is considered noninteractive at all occurances
						if (this.oConf.interaction !== false) {
							if (this.oConf.values[i] >= this.oConf.interaction ) {
								nonInteraction = false;
							}
						}
						// We have scrolled this far, but we haven't sent the event yet.

						// for now we assume it's properly configured. If no content is specified, we send % as value.
						// let content = this.oConf.content.length ? this.oConf.content[i] : this.oConf.values[i] + "%";

						window._jts.push({
							"track" : "event",
							"name" 	: "Scroll Depth",
							"noninteraction": nonInteraction,
							"group" : window.document.location.pathname,
							"value" : this.oConf.values[i] + "%",
							"intval" : this.oConf.values[i]
						});
	
						this.oConf.sent[i] = true;
					}
				}
	
				// Turn the event listener back on.
				window.addEventListener('scroll', this.handleScrollEvent)
			}.bind(this),500);
		}.bind(this);

		window.addEventListener('scroll', this.handleScrollEvent); // Activate Scrolling
	}.bind(this);

	this.setupConfiguration = function(oGateConfig) {
		var values = [];
		var content = [];
		var interaction = false;

		// delimiter is not required as we can also have only 1 value => f.e., at 50%
		if (!oGateConfig.delimiter) {
			values.push(oGateConfig.values);

			if (oGateConfig.content) {
				content.push(oGateConfig.content);
			}
		} else {
			values = oGateConfig.values.split(oGateConfig.delimiter);

			if (oGateConfig.content) {
				content = oGateConfig.content.split(oGateConfig.delimiter);
			}
		}

		if (typeof oGateConfig.interaction !== "undefined" && oGateConfig.interaction !== "") {
			interaction = oGateConfig.interaction;
		}

		return {
			"values": values,
			"content": content,
			"sent": [],
			"interaction": interaction
		};
	};
	
	this.init();
}